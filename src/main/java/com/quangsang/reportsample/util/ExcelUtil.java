package com.quangsang.reportsample.util;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


public class ExcelUtil {

    @Value("${filepath}")
    private String filePath;
    @Value("${sheetIndex}")
    private int sheetIndex;

    public static String readdatafromExcelusingcolumnName(String ColumnName)
            throws EncryptedDocumentException, InvalidFormatException, IOException, InvalidFormatException {
        String SheetName = "LoginDataSheet";
        File file = new File("C:/Users/Admin/Desktop/report-project/sample-xlsx-file.xlsx");
        FileInputStream fis = new FileInputStream(file);
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        // it will take value from first row
        Row row = sheet.getRow(0);
// it will give you count of row which is used or filled
        short lastcolumnused = row.getLastCellNum();

        int colnum = 0;
        for (int i = 0; i < lastcolumnused; i++) {
            if (row.getCell(i).getStringCellValue().equalsIgnoreCase(ColumnName)) {
                colnum = i;
                break;
            }
        }

        // it will take value from Second row
        row = sheet.getRow(1);
        Cell column = row.getCell(colnum);
        String CellValue = column.getStringCellValue();

        return CellValue;
    }

    public String readFileExcel(String path) throws IOException {
        FileInputStream fis = new FileInputStream(new File(path));
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);

        int rows = sheet.getLastRowNum();
        int columIndex = sheet.getLastRowNum();
        Row row1 = sheet.getRow(0);
        int lastColNumber = row1.getLastCellNum();

        List<String> list = new ArrayList<String>();
        for (int i = 0; i < lastColNumber; i++) {
            Row row = sheet.getRow(0);
            Cell cell = row.getCell(i);
            String rowHeader = cell.getStringCellValue().trim();
            list.add(rowHeader);
        }
        list.forEach(e -> {
            System.out.println("danh sach header:" + e);
        });
        System.out.println("<----------------------------------------------->");

        //System.out.println("getLastRowNum:" + rows);
        HashMap<String, List<String>> data = new HashMap<String, List<String>>();
        List<String> values;
        for (int k = 0; k <= columIndex; k++) {
            Row row = sheet.getRow(0);
            Cell cells = row.getCell(k);
            values = new ArrayList<String>();
            String rowHeader = cells.getStringCellValue().trim();
            for (int r = 1; r <= rows; r++) {
                int count = 0;
                Cell cell = sheet.getRow(r).getCell(k);
                String value = getCellValueAsString(cell);
                System.out.println(value);
                values.add(value);
            }
            //duyet
            //values
            data.put(rowHeader, values);
        }
        //duyet map
        System.out.println("");
        data.forEach((k, v) -> System.out.println(" " + k + " : " + v));
        return "";
    }



    public HashMap<String,List<String>> readFileExcels(String path) throws IOException {
        FileInputStream fis = new FileInputStream(new File(path));
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);

        int rows = sheet.getLastRowNum();
        int columIndex = sheet.getLastRowNum();
        int lastColNumber = sheet.getRow(0).getLastCellNum();

        List<String> list = new ArrayList<String>();
        for (int i = 0; i < lastColNumber; i++) {
            Row row = sheet.getRow(0);
            Cell cell = row.getCell(i);
            String rowHeader = cell.getStringCellValue().trim();
            list.add(rowHeader);
        }
//        list.forEach(e -> {
//            System.out.println("danh sach header:" + e);
//        });
//        System.out.println("<----------------------------------------------->");

        //System.out.println("getLastRowNum:" + rows);
        HashMap<String, List<String>> data = new HashMap<String, List<String>>();
        List<String> values;
//        for(int i=0;i < rows;i++){
//            Row row = sheet.getRow(0);
//            Cell cells = row.getCell(i < lastColNumber ? i : 0);
//            String rowHeader = cells.getStringCellValue();
//            values = new ArrayList<String>();
//            for(int j=1; j <= lastColNumber;j++){
//                String temp = "";
//                String aij = String.valueOf(sheet.getRow(i).getCell(j));
////                String aji  = String.valueOf(sheet.getRow(j).getCell(i));
////                temp = aij;
////                aij = aji;
////                aji = temp;
//                values.add(aij);
//            }
//            data.put(rowHeader, values);
//        }
        for (int j = 0; j < lastColNumber;j++){
            Row row = sheet.getRow(0);
            Cell cells = row.getCell(j);
            String rowHeader = cells.getStringCellValue();
            values = new ArrayList<String>();
            for(int i = 1; i < rows;i++){
                String aij = String.valueOf(sheet.getRow(i).getCell(j));
                values.add(aij);
            }
            data.put(rowHeader,values);
        }

//        String mane = String.valueOf(sheet.getRow(1).getCell(2));
//        String mane1 = String.valueOf(sheet.getRow(2).getCell(2));
//        System.out.println(mane+"/"+mane1);
        //duyet map
        System.out.println("");
        data.forEach((k, v) -> System.out.println(" " + k + " : " + v));
        return data;
    }

    public String readFileExcel1(String path) throws IOException {
        Map<String, List<String>> mapReport = new HashMap<>();
        String headerTemp = "";
        List<String> values = new ArrayList<>();
        String[] arrayValue = new String[values.size()];
        AtomicInteger columnIndex = new AtomicInteger();
        FileInputStream inputStream = new FileInputStream(new File(path));
        //tao workbook
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        //lay sheet
        XSSFSheet sheet = workbook.getSheetAt(0);
        int lastRowNumber = sheet.getLastRowNum();
        int lastColNumber = sheet.getRow(0).getLastCellNum();
        System.out.println("name sheet in file excel:" + sheet.getSheetName());
        System.out.println("--------------------------------------------------------------------------------");

        Iterator<Row> rowIterator1 = sheet.iterator();

        Iterator<Row> rowIterator = sheet.iterator();
        List<String> headers = getHeaders(rowIterator1);
        for (String h : headers) {

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.iterator();
                Cell header = null;
                if (row.getRowNum() == 0) {
                    while (cellIterator.hasNext()) {
                        header = cellIterator.next();
                        if (header.getStringCellValue().equals(h)) {
                            columnIndex.set(header.getColumnIndex());
                            headerTemp = header.getStringCellValue();
                            System.out.println("header:" + header.getStringCellValue() + "colum :" + columnIndex);
                        }
                    }
                } else {
                    Cell columCells = row.getCell(columnIndex.get());
                    System.out.println("value:" + columCells.getStringCellValue());
                    if (columCells != null) {
                        values.add(columCells.getStringCellValue());
                    }
                    assert header != null;
                    mapReport.put(headerTemp, values);
                }
            }

        }
        mapReport.forEach((k, v) -> System.out.println("Key : " + k + " Value : " + v));
        return "";
    }

    public List<Map<String, String>> readFileExcel2(String path) throws IOException {
        List<Map<String, String>> testDataAllRows = null;

        Map<String, String> testData = null;

        FileInputStream inputStream = new FileInputStream(new File(path));
        //tao workbook
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        //lay sheet
        XSSFSheet sheet = workbook.getSheetAt(0);
        int lastRowNumber = sheet.getLastRowNum();
        int lastColNumber = sheet.getRow(0).getLastCellNum();
        List list = new ArrayList();
        for (int i = 0; i < lastColNumber; i++) {
            Row row = sheet.getRow(0);
            Cell cell = row.getCell(i);
            String rowHeader = cell.getStringCellValue().trim();
            list.add(rowHeader);
        }

        testDataAllRows = new ArrayList<Map<String, String>>();
        for (int j = 1; j <= lastRowNumber; j++) {
            Row row = sheet.getRow(j);
            testData = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
            for (int k = 0; k < lastColNumber; k++) {
                Cell cell = row.getCell(k);
                //String colValue = cell.getStringCellValue().trim();
                String colValue = null;
                switch (cell.getCellTypeEnum()) {
                    case NUMERIC:
                        colValue = String.valueOf(cell.getNumericCellValue());
                        break;
                    case STRING:
                        colValue = cell.getStringCellValue();
                        break;
                    case BOOLEAN:
                        colValue = String.valueOf(cell.getBooleanCellValue());
                        break;
                    case FORMULA:
                        colValue = cell.getCellFormula();
                    case BLANK:
                        colValue = "BLANK";
                    default:
                        colValue = "DEFAULT";
                }
                testData.put((String) list.get(k), colValue);
            }
            testDataAllRows.add(testData);
        }
        testDataAllRows.forEach(System.out::println);
        return testDataAllRows;
    }

    public Map<String, Map<String, String>> readeFileExcel3(String path) throws IOException {
        Map<String, Map<String, String>> completeSheetData = new HashMap<String, Map<String, String>>();
        List<String> columnHeader = new ArrayList<String>();
        Map<String, String> rowData;

        FileInputStream inputStream = new FileInputStream(new File(path));
        //tao workbook
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        //lay sheet
        XSSFSheet sheet = workbook.getSheetAt(0);


        Row row = sheet.getRow(0);
        Iterator<Cell> cellIterator = row.cellIterator();
        while (cellIterator.hasNext()) {
            columnHeader.add(cellIterator.next().getStringCellValue());
        }
        int rowCount = row.getLastCellNum();
        int columnCount = row.getLastCellNum();
        for (int i = 0; i < rowCount; i++) {// <= la bi null point
            Map<String, String> singleRowData = new HashMap<String, String>();
            Row row1 = sheet.getRow(i);
            for (int j = 0; j < columnCount; j++) {
                Cell cell = row1.getCell(j);
                singleRowData.put(columnHeader.get(j), getCellValueAsString(cell));
            }
            //completeSheetData.put(String.valueOf(i), singleRowData);
            completeSheetData.put(columnHeader.get(i), singleRowData);
        }
        completeSheetData.forEach((k, v) -> System.out.println("Key : " + k + " Value : " + v));
        return completeSheetData;
    }

    public Map<String, List<String>> readeFileExcel4(String path) throws IOException {
        Map<String, List<String>> completeSheetData = new HashMap<String, List<String>>();
        List<String> columnHeader = new ArrayList<String>();

        Map<String, String> rowData;

        FileInputStream inputStream = new FileInputStream(new File(path));
        //tao workbook
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        //lay sheet
        XSSFSheet sheet = workbook.getSheetAt(0);


        Row row = sheet.getRow(0);
        Iterator<Cell> cellIterator = row.cellIterator();
        while (cellIterator.hasNext()) {
            columnHeader.add(cellIterator.next().getStringCellValue());
        }
        int rowCount = row.getLastCellNum();
        int columnCount = row.getLastCellNum();
        for (int i = 1; i < rowCount; i++) {// <= la bi null point
            //Map<String, String> singleRowData = new HashMap<String, String>();
            List<String> singleRowData = new ArrayList<>();
            Row row1 = sheet.getRow(i);
            for (int j = 0; j < columnCount; j++) {
                Cell cell = row1.getCell(j);
                singleRowData.add(getCellValueAsString(cell));
            }
            completeSheetData.put(String.valueOf(i), singleRowData);
        }
        completeSheetData.forEach((k, v) -> System.out.println("Key : " + k + " Value : " + v));
        return completeSheetData;
    }

    //get header in file Excel
    public List<String> getHeaders(Iterator<Row> rowIterator) {
        List<String> headers = new ArrayList<>();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.iterator();
            Cell header = null;
            if (row.getRowNum() == 0) {
                while (cellIterator.hasNext()) {
                    header = cellIterator.next();
                    headers.add(header.getStringCellValue());
                }
            }
        }
        return headers;
    }

    private HSSFSheet getSheet() throws IOException {
        FileInputStream fis = new FileInputStream(new File(filePath));
        HSSFWorkbook workbook = new HSSFWorkbook(fis);
        HSSFSheet sheet = workbook.getSheetAt(sheetIndex);
        return sheet;
    }

    public static String getCellValueAsString(Cell cell) {
        String cellValue = null;
        switch (cell.getCellTypeEnum()) {
            case NUMERIC:
                cellValue = String.valueOf(cell.getNumericCellValue());
                break;
            case STRING:
                cellValue = cell.getStringCellValue();
                break;
            case BOOLEAN:
                cellValue = String.valueOf(cell.getBooleanCellValue());
                break;
            case FORMULA:
                cellValue = cell.getCellFormula();
            case BLANK:
                cellValue = "BLANK";
            default:
                cellValue = "DEFAULT";
        }
        return cellValue;
    }

    public String getSheetName(int index) throws IOException {
        FileInputStream file = new FileInputStream(filePath);
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        String sheet = workbook.getSheetName(index);
        return sheet;
    }

    public int getSheetCount() throws IOException {
        FileInputStream file = new FileInputStream(filePath);
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        int sheetCount = workbook.getNumberOfSheets();
        return sheetCount;
    }

    public int totolColumnCount() throws IOException {
        HSSFSheet sheet = getSheet();
        Row row = sheet.getRow(0);
        int lastColumnNum = row.getLastCellNum();
        return lastColumnNum;
    }

    public void convertCSVtoExcel() {

    }


    public static void main(String[] args) throws IOException, InvalidFormatException {
        ExcelUtil util = new ExcelUtil();
        String path = "C:/Users/Admin/Desktop/report-project/output.xlsx";
        //String path = "C:/Users/Admin/Desktop/report-project/f_kpi_sector_by_day_VPC_2019.csv";
        util.readFileExcels(path);
//        String value = util.readFileExcel(path);
//        System.out.println("cell: " + value);
    }
}
