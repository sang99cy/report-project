package com.quangsang.reportsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReportSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReportSampleApplication.class, args);
    }

}
