package com.quangsang.reportsample.service;

import com.quangsang.reportsample.util.ExcelUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

@Service
public class ReportService {

    public HashMap<String,List<String>> readFileExcels(String path) throws IOException {
        FileInputStream fis = new FileInputStream(new File(path));
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);

        int rows = sheet.getLastRowNum();
        int columIndex = sheet.getLastRowNum();
        int lastColNumber = sheet.getRow(0).getLastCellNum();

        List<String> list = new ArrayList<String>();
        for (int i = 0; i < lastColNumber; i++) {
            Row row = sheet.getRow(0);
            Cell cell = row.getCell(i);
            String rowHeader = cell.getStringCellValue().trim();
            list.add(rowHeader);
        }

        HashMap<String, List<String>> data = new HashMap<String, List<String>>();
        List<String> values;

        for (int j = 0; j < lastColNumber;j++){
            Row row = sheet.getRow(0);
            Cell cells = row.getCell(j);
            String rowHeader = cells.getStringCellValue();
            values = new ArrayList<String>();
            for(int i = 1; i < rows;i++){
                String aij = String.valueOf(sheet.getRow(i).getCell(j));
                values.add(aij);
            }
            data.put(rowHeader,values);
        }

        //duyet map
//        System.out.println("");
//        data.forEach((k, v) -> System.out.println(" " + k + " : " + v));
        return data;
    }



    public Map<String, Map<String, String>> readeFileExcel3(String path) throws IOException {
        Map<String, Map<String, String>> completeSheetData = new HashMap<String, Map<String, String>>();
        List<String> columnHeader = new ArrayList<String>();

        Map<String, String> rowData;

        FileInputStream inputStream = new FileInputStream(new File(path));
        //tao workbook
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        //lay sheet
        XSSFSheet sheet = workbook.getSheetAt(0);


        Row row = sheet.getRow(0);
        Iterator<Cell> cellIterator = row.cellIterator();
        while (cellIterator.hasNext()) {
            columnHeader.add(cellIterator.next().getStringCellValue());
        }
        int rowCount = row.getLastCellNum();
        int columnCount = row.getLastCellNum();
        for (int i = 1; i < rowCount; i++) {// <= la bi null point
            Map<String, String> singleRowData = new HashMap<String, String>();
            Row row1 = sheet.getRow(i);
            for (int j = 0; j < columnCount; j++) {
                Cell cell = row1.getCell(j);
                singleRowData.put(columnHeader.get(j), ExcelUtil.getCellValueAsString(cell));
            }
            completeSheetData.put(String.valueOf(i), singleRowData);
        }
        completeSheetData.forEach((k, v) -> System.out.println("Key : " + k + " Value : " + v));
        return completeSheetData;
    }

    public Map<String, List<String>> readeFileExcel4(String path) throws IOException {
        Map<String, List<String>> completeSheetData = new HashMap<String, List<String>>();
        List<String> columnHeader = new ArrayList<String>();

        Map<String, String> rowData;

        FileInputStream inputStream = new FileInputStream(new File(path));
        //tao workbook
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        //lay sheet
        XSSFSheet sheet = workbook.getSheetAt(0);

        Row row = sheet.getRow(0);
        Iterator<Cell> cellIterator = row.cellIterator();

        String title = "";
        while (cellIterator.hasNext()) {
            //Cell header = cellIterator.next();

            Cell header = null;
            if (row.getRowNum() == 0) {
                while (cellIterator.hasNext()) {
                    header = cellIterator.next();
                    title = header.getStringCellValue();
                    columnHeader.add(title);
                }
            }
            //columnHeader.add(cellIterator.next().getStringCellValue());
        }
        int rowCount = row.getLastCellNum();
        int columnCount = row.getLastCellNum();
        for (int i = 1; i < rowCount; i++) {// <= la bi null point
            //Map<String, String> singleRowData = new HashMap<String, String>();
            List<String> singleRowData = new ArrayList<>();
            Row row1 = sheet.getRow(i);
            for (int j = 0; j < columnCount; j++) {
                Cell cell = row1.getCell(j);
                singleRowData.add(ExcelUtil.getCellValueAsString(cell));
            }
            //completeSheetData.put(columnHeader.get(i), singleRowData);
            completeSheetData.put(String.valueOf(i), singleRowData);
        }
        completeSheetData.forEach((k, v) -> System.out.println("Key : " + k + " Value : " + v));
        return completeSheetData;
    }

    public static HashMap loadExcelLines(File fileName) {
        // Used the LinkedHashMap and LikedList to maintain the order
        HashMap<String, LinkedHashMap<Integer, List>> outerMap = new LinkedHashMap<String, LinkedHashMap<Integer, List>>();

        LinkedHashMap<Integer, List> hashMap = new LinkedHashMap<Integer, List>();

        String sheetName = null;
        // Create an ArrayList to store the data read from excel sheet.
        // List sheetData = new ArrayList();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fileName);
            // Create an excel workbook from the file system
            XSSFWorkbook workBook = new XSSFWorkbook(fis);
            // Get the first sheet on the workbook.
            for (int i = 0; i < workBook.getNumberOfSheets(); i++) {
                XSSFSheet sheet = workBook.getSheetAt(i);
                // XSSFSheet sheet = workBook.getSheetAt(0);
                sheetName = workBook.getSheetName(i);

                Iterator rows = sheet.rowIterator();
                while (rows.hasNext()) {
                    XSSFRow row = (XSSFRow) rows.next();
                    Iterator cells = row.cellIterator();

                    List data = new LinkedList();
                    while (cells.hasNext()) {
                        XSSFCell cell = (XSSFCell) cells.next();
                       //cell.setCellType(Cell.CELL_TYPE_STRING);
                        String finalCell = ExcelUtil.getCellValueAsString(cell);
                        data.add(finalCell);
                    }
                    hashMap.put(row.getRowNum(), data);

                    // sheetData.add(data);
                }
                outerMap.put(sheetName, hashMap);
                hashMap = new LinkedHashMap<Integer, List>();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        return outerMap;

    }
}
