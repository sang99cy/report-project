package com.quangsang.reportsample.controller;

import com.quangsang.reportsample.service.ReportService;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/api/report/")
public class ReportController {

    private final ReportService reportService;

    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("excel")
    public Map<String, String[]> ReportExcel() {
        Map<String, String[]> reports = new HashMap<>();
        String[] values1 = {"sang", "huong", "trang"};
        String[] values2 = {"04071999", "09111996", "22052005"};
        String[] values3 = {"hanoi", "hanoi", "hanoi"};
        reports.put("Name", values1);
        reports.put("Date", values2);
        reports.put("Address", values3);
        return reports;
    }

    @GetMapping("/excel1")
    public Map<String, List<String>> ReportExcel1() {
        Map<String,List<String>> reports = new HashMap<>();
        List<String> listName = new ArrayList<>();
        listName.add("sang");
        listName.add("huong");
        listName.add("trang");
        List<String> listDate = new ArrayList<>();
        listDate.add("04071999");
        listDate.add("09111996");
        listDate.add("22052005");
        reports.put("Name",listName);
        reports.put("Date",listDate);
        return reports;
    }
    @GetMapping("/excel3")
    public Map<String, Map<String, String>> ReportExcel3() throws IOException {
        String path = "C:\\Users\\Admin\\Desktop\\report-project\\sample-xlsx-file.xlsx";
        Map<String, Map<String, String>> reports = reportService.readeFileExcel3(path);
        return reports;
    }

    @GetMapping("/excel4")
    public Map<String, List<String>> ReportExcel4() throws IOException {
        String path = "C:\\Users\\Admin\\Desktop\\report-project\\sample-xlsx-file.xlsx";
        Map<String,List<String>> reports = reportService.readeFileExcel4(path);
        return reports;
    }

    @GetMapping("/excel5")
    public HashMap<String, LinkedHashMap<Integer, List>> ReportExcel5() throws IOException {
        String path = "C:\\Users\\Admin\\Desktop\\report-project\\sample-xlsx-file.xlsx";
        HashMap<String, LinkedHashMap<Integer, List>> reports = reportService.loadExcelLines(new File(path));
        return reports;
    }

    @GetMapping("/excels")
    public HashMap<String, List<String>> ReportExcels() throws IOException {
        String path = "C:\\Users\\Admin\\Desktop\\report-project\\output.xlsx";
        HashMap<String,List<String>> data = reportService.readFileExcels(path);
        return data;
    }
}
